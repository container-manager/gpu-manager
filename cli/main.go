package main

import (
	"encoding/json"
	"fmt"
	arg "github.com/alexflint/go-arg"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Gpu struct {
	Name                  string
	Idx                   uint
	Addr                  string
	ContainerName         string
	InternalContainerName string
	UserName              string
	LastUsedTime          time.Time
	Lock                  bool
}

var rootCmd = &cobra.Command{
	Use:   "gpu",
	Short: "Manage GPU's attached to your container",
}

var getCmd = &cobra.Command{
	Use:                "get",
	Short:              "Attaches GPUs to your container",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		var getArgs struct {
			RestartOnTimeout bool `help:"Restart on timeout" arg:"-r,--restart-on-timeout" url:"restart-on-timeout"`
			Timeout  string `help:"Override default timeout (5m)" arg:"-t,--timeout" url:"timeout"`
			Lock bool `help:"(DEPRECATED) Lock GPUs" url:"lock"`
			Count uint `arg:"positional,required" url:"count" help:"Number of GPUs to get`
		}
		MustParse(&getArgs, args, "gpu get")

		if getArgs.Lock {
			fmt.Println("WARN: gpu get --lock is deprecated")
		}

		code, res, err := postStruct("/get", getArgs)
		handleResponse(code, res, err)
	},
}

var waitCmd = &cobra.Command{
	Use:                "wait",
	Short:              "Attaches GPUs to your container",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		var getArgs struct {
			Count uint `arg:"positional,required" url:"count" help:"Number of GPUs to get`
		}
		MustParse(&getArgs, args, "gpu wait")

		code, res, err := postStruct("/wait", getArgs)
		handleResponse(code, res, err)
	},
}

var releaseCmd = &cobra.Command{
	Use:                "release",
	Short:              "Releases GPUs from your container",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		code, res, err := postReq("/release")
		handleResponse(code, res, err)
	},
}

var infoCmd = &cobra.Command{
	Use:                "info",
	Short:              "Prints info about GPU leases (inactive time shown in parentheses)",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		_, res, err := postReq("/leases")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
			return
		}
		var leases []Gpu
		json.Unmarshal(res, &leases)
		printInfoTable(leases)
	},
}

var doneCmd = &cobra.Command{
	Use:                "done",
	Short:              "Sends you a done message on slack",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		code, res, err := postReq("/done")
		handleResponse(code, res, err)
	},
}

var smiCmd = &cobra.Command{
	Use:                "smi",
	Short:              "Shows nvidia-smi output from host",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		code, res, err := postReq("/smi")
		handleResponse(code, res, err)
	},
}

var smiTopoCmd = &cobra.Command{
	Use:                "smi-topo",
	Short:              "Shows nvidia-smi topo -m output from host",
	DisableFlagParsing: true,
	Run: func(cmd *cobra.Command, args []string) {
		code, res, err := postReq("/smi/topo")
		handleResponse(code, res, err)
	},
}

func handleResponse(code int, response []byte, err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
	} else if code == http.StatusOK {
		resStr := string(response)
		if resStr == "" {
			fmt.Println("OK")
		} else {
			fmt.Println(resStr)
		}
	} else {
		fmt.Fprintf(os.Stderr, "Error (%d): %s\n", code, response)
	}
}

// fmtDurationShort formats a duration to it's shortest rounded representation
// 10m15s -> 10m
// 11h10m12s -> 11h
// 1d10h1m11s -> 1d
func fmtDurationShort(t time.Duration, min string) string {
	oneDay := time.Hour * 24
	d := t / oneDay
	t -= d * oneDay
	h := t / time.Hour
	t -= h * time.Hour
	m := t / time.Minute
	t -= m * time.Minute
	s := t / time.Second
	if d != 0 {
		return strconv.Itoa(int(d)) + "d"
	} else if min == "d" {
		return ""
	}
	if h != 0 {
		return strconv.Itoa(int(h)) + "h"
	} else if min == "h" {
		return ""
	}
	if m != 0 {
		return strconv.Itoa(int(m)) + "m"
	} else if min == "m" {
		return ""
	}
	if s != 0 {
		return strconv.Itoa(int(s)) + "s"
	}
	return ""
}

func printInfoTable(leases []Gpu) {
	if len(leases) == 0 {
		fmt.Println("No active GPU leases")
	}

	leaseMap := make(map[string][]Gpu)

	for _, lease := range leases {
		name := lease.InternalContainerName
		leaseMap[name] = append(leaseMap[name], lease)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Container", "User", "GPU"})
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetAutoWrapText(false)

	for _, leases := range leaseMap {
		name := leases[0].ContainerName
		userName := leases[0].UserName
		var gpuIdxs []string
		for _, lease := range leases {
			gpuIdx := strconv.FormatUint(uint64(lease.Idx), 10)
			if lease.Lock {
				gpuIdx = fmt.Sprintf("\033[1m%s\033[0m", gpuIdx)
			}
			inactiveTime := time.Since(lease.LastUsedTime)
			t := fmtDurationShort(inactiveTime, "h")
			if t != "" {
				gpuIdx = fmt.Sprintf("%s (%s)", gpuIdx, t)
			}
			gpuIdxs = append(gpuIdxs, gpuIdx)
		}
		row := []string{name, userName, strings.Join(gpuIdxs, ", ")}
		table.Append(row)
	}

	table.Render()
}

func MustParse(dest interface{}, args []string, name string) {
	parser, _ := arg.NewParser(arg.Config{Program: name}, dest)
	err := parser.Parse(args)
	if err != nil {
		if err != arg.ErrHelp {
			fmt.Fprintf(os.Stderr, "Error: %s\n\n", err.Error())
		}
		parser.WriteHelp(os.Stderr)
		os.Exit(1)
	}
}

func main() {
	rootCmd.AddCommand(getCmd)
	rootCmd.AddCommand(releaseCmd)
	rootCmd.AddCommand(waitCmd)
	rootCmd.AddCommand(infoCmd)
	rootCmd.AddCommand(doneCmd)
	rootCmd.AddCommand(smiCmd)
	rootCmd.AddCommand(smiTopoCmd)
	rootCmd.Execute()
}
