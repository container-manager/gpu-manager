package main

import (
	"net"
	"net/http"
	"os"
	"context"
	"fmt"
	"io/ioutil"
	"github.com/google/go-querystring/query"
)

func getSockPath() string {
	path := "/dev/gpu-manager/socket"
	env := os.Getenv("GPUMANAGER_SOCK")
	if env != "" {
		path = env
	}
	return path
}

func makeClient() http.Client {
	path := getSockPath()

	httpc := http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				return net.Dial("unix", path)
			},
		},
	}

	return httpc
}

func postReq(url string) (int, []byte, error) {
	httpc := makeClient()
	response, err := httpc.Get("http://unix" + url)
	if err != nil {
		return 0, nil, err
	}
	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)
	return response.StatusCode, bodyBytes, nil
}

func postStruct(path string, q interface{}) (int, []byte, error) {
	v, _ := query.Values(q)

	url := fmt.Sprintf("%s?%s", path, v.Encode())
	return postReq(url)
}
