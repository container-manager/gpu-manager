package main

func MakeRange(min, max uint) []uint {
	a := make([]uint, max-min+1)
	for i := range a {
		a[i] = min + uint(i)
	}
	return a
}
