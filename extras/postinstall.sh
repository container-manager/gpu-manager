#!/bin/bash
useradd -ms /bin/bash gpu
usermod -aG lxd gpu

systemctl daemon-reload
systemctl enable gpu-manager.socket
systemctl start gpu-manager.socket

lxc profile create managed-container || true
lxc profile device add managed-container gpu-sock disk source=/run/gpu-manager/socket path=/dev/gpu-manager/socket || true
