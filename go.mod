module gitlab.com/container-manager/gpu-manager

require (
	github.com/NVIDIA/gpu-monitoring-tools v0.0.0-20200116003318-021662a21098
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/flosch/pongo2 v0.0.0-20200529170236-5abacdfa4915 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lxc/lxd v0.0.0-20200618195219-eb84e347b5c5
	github.com/nlopes/slack v0.6.0
	github.com/shirou/gopsutil v2.20.5+incompatible
	gitlab.com/gartnera/golib v0.0.0-20190619234814-0708a17f76de
	golang.org/x/sys v0.0.0-20200615200032-f1bc736245b1
	gopkg.in/macaroon-bakery.v2 v2.2.0 // indirect
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5 // indirect
)

go 1.13
