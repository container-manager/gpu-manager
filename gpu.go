package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/NVIDIA/gpu-monitoring-tools/bindings/go/nvml"
	"github.com/lxc/lxd/client"
	lxdApi "github.com/lxc/lxd/shared/api"

	"gitlab.com/gartnera/golib/log"
)

// TODO: lxd events api on rm or shutdown

const (
	StatePath = "/var/tmp/gpu-manager-leases"
)

type Gpu struct {
	Name string
	Idx  uint
	Addr string

	Devices []string

	// These fields are only set on leased GPUs
	ContainerName         string
	InternalContainerName string
	UserName              string
	LastUsedTime          time.Time
	Lock                  bool
	RestartOnTimeout      bool
	Duration              time.Duration
	didTimeout            bool
}

func (g *Gpu) saveDeviceNames() {
	nvidiaPath := fmt.Sprintf("/dev/nvidia%d", g.Idx)
	linkFmt := "/dev/dri/by-path/pci-%s-%s"
	cardLinkPath := fmt.Sprintf(linkFmt, g.Addr, "card")
	renderLinkPath := fmt.Sprintf(linkFmt, g.Addr, "render")

	cardPath, err := os.Readlink(cardLinkPath)
	if err != nil {
		log.Error("unable to readlink " + cardLinkPath)
	}

	renderPath, err := os.Readlink(renderLinkPath)
	if err != nil {
		log.Error("unable to readlink " + renderPath)
	}

	g.Devices = []string{nvidiaPath, cardPath, renderPath}
}

type GpuState struct {
	totalGpuCount   uint
	AvaliableGpus   []*Gpu
	LeasedGpus      []*Gpu
	lock            sync.RWMutex
	waitCondition   *sync.Cond
	timeoutDuration time.Duration
	statsManager    *StatsManager
}

func (s *GpuState) organizeGpus() {
	a := s.AvaliableGpus
	sort.Slice(a, func(i, j int) bool { return a[i].Idx < a[j].Idx })

	for _, gpu := range a {
		gpu.ContainerName = ""
		gpu.InternalContainerName = ""
		gpu.UserName = ""
	}

	l := s.LeasedGpus
	sort.Slice(l, func(i, j int) bool { return l[i].Idx < l[j].Idx })

	bytes, _ := json.Marshal(l)
	ioutil.WriteFile(StatePath, bytes, 500)
}

func (s *GpuState) getContainerGpus(containerName string) []*Gpu {
	var res []*Gpu
	for _, gpu := range s.LeasedGpus {
		if gpu.InternalContainerName == containerName {
			res = append(res, gpu)
		}
	}
	return res
}

// caller must lock
func (s *GpuState) releaseGpus(gpus []*Gpu) error {
	// If releasing multiple GPUs, we assume the InternalContainerName is the same for all Gpu's
	containerName := gpus[0].InternalContainerName
	gpusToRelease, gpusWereFiltered := filterGpusInUse(gpus)

	if len(gpusToRelease) == 0 && gpusWereFiltered {
		return errors.New("can't release GPUs in use")
	}

	server, err := lxd.ConnectLXDUnix("", nil)
	if err != nil {
		return err
	}

	container, etag, err := server.GetContainer(containerName)
	if err != nil {
		return err
	}

	var mountsToSweep []string
	restartOnTimeout := false
	didTimeout := false

	devices := container.Devices
	for _, gpu := range gpusToRelease {
		restartOnTimeout = gpu.RestartOnTimeout
		didTimeout = gpu.didTimeout
		mountsToSweep = append(mountsToSweep, gpu.Devices...)
		name := gpu.Name
		if _, ok := devices[name]; ok {
			delete(devices, name)
		}
	}

	op, err := server.UpdateContainer(containerName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	if !gpusWereFiltered && restartOnTimeout && didTimeout {
		restartReq := lxdApi.ContainerStatePut{
			Action:  "restart",
			Force:   true,
			Timeout: 10,
		}

		op, err := server.UpdateContainerState(containerName, restartReq, "")
		if err != nil {
			return err
		}

		err = op.Wait()
		if err != nil {
			return err
		}
	} else {
		sweepUmount(mountsToSweep)
	}

	s.softGpuRelease(gpusToRelease)

	if gpusWereFiltered {
		return errors.New("partial GPU release performed because some GPUs in use")
	}

	return nil
}

// softGpuRelease releases GPUs without involving lxd
// used during normal operations and to clear leases when a container is deleted
func (s *GpuState) softGpuRelease(gpus []*Gpu) {
	gpuState.statsManager.releaseGpus <- gpus

	gpuState.AvaliableGpus = append(gpuState.AvaliableGpus, gpus...)

	oldLeases := gpuState.LeasedGpus
	var newLeases []*Gpu
	for _, leasedGPU := range oldLeases {
		keep := true
		for _, gpu := range gpus {
			if gpu.Addr == leasedGPU.Addr {
				keep = false
			}
		}
		if keep {
			newLeases = append(newLeases, leasedGPU)
		}
	}
	gpuState.LeasedGpus = newLeases

	gpuState.waitCondition.Signal()

	gpuState.organizeGpus()
}

var gpuState *GpuState

func InitGpuManager(statsManager *StatsManager) error {
	err := nvml.Init()
	if err != nil {
		return err
	}
	count, err := nvml.GetDeviceCount()
	if err != nil {
		log.Panicln("Error getting device count:", err)
	}
	var gpuNames []string
	var avaliableGpus []*Gpu
	for i := uint(0); i < count; i++ {
		device, _ := nvml.NewDevice(i)
		name := fmt.Sprintf("gpu%d", i)
		addr := device.PCI.BusID[4:]
		addr = strings.ToLower(addr)
		gpu := &Gpu{
			Name: name,
			Idx:  i,
			Addr: addr,
		}
		gpu.saveDeviceNames()
		gpuNames = append(gpuNames, *device.Model)
		avaliableGpus = append(avaliableGpus, gpu)
	}
	statsManager.initGpu <- gpuNames

	waitCondition := sync.NewCond(&sync.Mutex{})
	gpuState = &GpuState{
		totalGpuCount:   count,
		AvaliableGpus:   avaliableGpus,
		waitCondition:   waitCondition,
		timeoutDuration: getTimeoutDuration(),
		statsManager:    statsManager,
	}
	recoverGPUState()
	go handleGpuTimeout()
	go handleContainerEvents()
	return nil
}

func ShutdownGpuManager() {
	nvml.Shutdown()
}

func AttachGpus(containerName string, count uint, duration time.Duration, restartOnTimeout bool, lock bool) error {
	gpuState.lock.Lock()
	defer gpuState.lock.Unlock()

	avaliableGpus := gpuState.AvaliableGpus
	avaliableGpuCount := uint(len(avaliableGpus))
	if count > avaliableGpuCount {
		return fmt.Errorf("only %d GPUs avaliable", avaliableGpuCount)
	}

	server, err := lxd.ConnectLXDUnix("", nil)
	if err != nil {
		return err
	}

	container, etag, err := server.GetContainer(containerName)
	if err != nil {
		return err
	}
	userName := container.Config["user.username"]
	remoteName := container.Config["user.remotename"]

	gpusToAttach := gpuState.AvaliableGpus[:count]
	newAvaliableGpus := gpuState.AvaliableGpus[count:]

	for _, gpu := range gpusToAttach {
		gpu.InternalContainerName = containerName
		gpu.ContainerName = remoteName
		gpu.UserName = userName
		gpu.LastUsedTime = time.Now()
		gpu.Lock = lock
		gpu.Duration = duration
		gpu.RestartOnTimeout = restartOnTimeout
		container.Devices[gpu.Name] = map[string]string{
			"type": "gpu",
			"pci":  gpu.Addr,
		}
	}

	op, err := server.UpdateContainer(containerName, container.Writable(), etag)
	if err != nil {
		return err
	}

	err = op.Wait()
	if err != nil {
		return err
	}

	gpuState.statsManager.attachGpus <- gpusToAttach

	gpuState.LeasedGpus = append(gpuState.LeasedGpus, gpusToAttach...)
	gpuState.AvaliableGpus = newAvaliableGpus

	gpuState.organizeGpus()
	return nil
}

func WaitGpus(containerName string, count uint, w http.ResponseWriter) error {
	gpuState.lock.RLock()
	totalGpus := gpuState.totalGpuCount
	avaliableGpus := gpuState.AvaliableGpus
	avaliableGpuCount := uint(len(avaliableGpus))
	gpuState.lock.RUnlock()

	if count > totalGpus {
		return fmt.Errorf("only %d total GPUs", totalGpus)
	}

	gpus := gpuState.getContainerGpus(containerName)
	if len(gpus) != 0 {
		err := gpuState.releaseGpus(gpus)
		if err != nil {
			return err
		}
	}

	if avaliableGpuCount >= count {
		return AttachGpus(containerName, count, getDefaultDuration(), false, false)
	}

	closeNotifier := w.(http.CloseNotifier).CloseNotify()

	for {
		gpuState.waitCondition.L.Lock()
		gpuState.waitCondition.Wait()
		gpuState.waitCondition.L.Unlock()

		select {
		case <-closeNotifier:
			gpuState.waitCondition.Signal()
			return nil
		default:
		}

		gpuState.lock.RLock()
		avaliableGpus = gpuState.AvaliableGpus
		avaliableGpuCount = uint(len(avaliableGpus))
		gpuState.lock.RUnlock()

		if avaliableGpuCount >= count {
			return AttachGpus(containerName, count, getDefaultDuration(), false, false)
		} else {
			gpuState.waitCondition.Signal()
		}

	}
}

func ReleaseGpus(containerName string) error {
	gpuState.lock.Lock()
	defer gpuState.lock.Unlock()
	gpus := gpuState.getContainerGpus(containerName)
	if len(gpus) == 0 {
		return errors.New("no GPUs to release")
	}
	return gpuState.releaseGpus(gpus)
}

func GetLeasesJSON() []byte {
	gpuState.lock.RLock()
	defer gpuState.lock.RUnlock()

	bytes, _ := json.Marshal(gpuState.LeasedGpus)
	return bytes
}

func GetHostSmiOutput() ([]byte, error) {
	cmd := exec.Command("nvidia-smi")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func GetHostSmiTopoOutput() ([]byte, error) {
	cmd := exec.Command("nvidia-smi", "topo", "-m")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func getDefaultDuration() time.Duration {
	return time.Minute * 5
}

func filterGpusInUse(gpus []*Gpu) ([]*Gpu, bool) {
	var res []*Gpu
	didFilter := false
	for _, gpu := range gpus {
		device, err := nvml.NewDevice(gpu.Idx)
		if err != nil {
			continue
		}
		pInfo, err := device.GetAllRunningProcesses()
		if err != nil {
			continue
		}
		if len(pInfo) > 0 {
			didFilter = true
		} else {
			res = append(res, gpu)
		}
	}
	return res, didFilter
}

func getTimeoutDuration() time.Duration {
	timeStr := "300"
	env := os.Getenv("GPUMANAGER_TIMEOUT")
	if env != "" {
		timeStr = env
	}

	seconds, err := strconv.Atoi(timeStr)
	if err != nil {
		panic(err)
	}

	return time.Second * time.Duration(seconds)
}

func handleGpuTimeoutLoop() {
	var leasesToRelease []*Gpu
	for _, lease := range gpuState.LeasedGpus {
		now := time.Now()
		if areGpusInUse(lease) {
			lease.LastUsedTime = now
		} else if lease.Lock {
			continue
		} else if now.Sub(lease.LastUsedTime) > lease.Duration {
			log.WithFields(log.Fields{
				"container": lease.InternalContainerName,
				"idx":       lease.Idx,
			}).Info("GPU timeout")
			leasesToRelease = append(leasesToRelease, lease)
			lease.didTimeout = true
		}
	}
	for _, lease := range leasesToRelease {
		gpuState.releaseGpus([]*Gpu{lease})
	}
}

func handleGpuTimeout() {
	for {
		time.Sleep(time.Second * 10)
		gpuState.lock.Lock()
		handleGpuTimeoutLoop()
		gpuState.waitCondition.Signal()
		gpuState.lock.Unlock()
	}
}

func areGpusInUse(gpus ...*Gpu) bool {
	for _, gpu := range gpus {
		device, err := nvml.NewDevice(gpu.Idx)
		if err != nil {
			return true
		}
		pInfo, err := device.GetAllRunningProcesses()
		if err != nil {
			return true
		}
		if len(pInfo) > 0 {
			return true
		}
	}
	return false
}

func clearAllGpus() error {
	server, err := lxd.ConnectLXDUnix("", nil)
	if err != nil {
		return err
	}
	containers, err := server.GetContainers()
	for _, c := range containers {
		container, etag, err := server.GetContainer(c.Name)
		if err != nil {
			return err
		}

		devices := container.Devices
		for key := range devices {
			if strings.HasPrefix(key, "gpu") {
				delete(devices, key)
			}
		}

		op, err := server.UpdateContainer(c.Name, container.Writable(), etag)
		if err != nil {
			return err
		}

		err = op.Wait()
		if err != nil {
			return err
		}
	}
	return nil
}

func handleContainerEvents() {
	for {
		server, err := lxd.ConnectLXDUnix("", nil)
		if err != nil {
			panic(err)
		}

		events, err := server.GetEvents()
		if err != nil {
			panic(err)
		}

		events.AddHandler([]string{"lifecycle"}, func(evt lxdApi.Event) {
			lifeEvent := &lxdApi.EventLifecycle{}
			err := json.Unmarshal(evt.Metadata, lifeEvent)
			if err != nil {
				return
			}
			action := lifeEvent.Action
			if action != "container-deleted" {
				return
			}
			sourceParts := strings.Split(lifeEvent.Source, "/")
			containerName := sourceParts[3]
			log.WithField("container", containerName).Info("releasing GPUs because of delete event")
			gpuState.lock.Lock()
			defer gpuState.lock.Unlock()
			gpus := gpuState.getContainerGpus(containerName)
			gpuState.softGpuRelease(gpus)
		})
		events.Wait()
		log.Warn("lxd server disconnected. Reconnecting...")
		time.Sleep(time.Second * 10)
	}
}

func recoverGPUState() {
	bytes, err := ioutil.ReadFile(StatePath)
	if err != nil {
		log.Warn("no existing state, clearing all lxd GPUs")
		clearAllGpus()
		return
	}

	var oldLeased []*Gpu
	err = json.Unmarshal(bytes, &oldLeased)
	if err != nil {
		log.Warn("unable to parse gpu state, clearing all lxd GPUs")
		clearAllGpus()
		return
	}

	avaliableGpus := gpuState.AvaliableGpus
	var newLeased []*Gpu
	for _, oldGpu := range oldLeased {
		for i, gpu := range avaliableGpus {
			if gpu.Addr == oldGpu.Addr {
				newLeased = append(newLeased, oldGpu)
				avaliableGpus = append(avaliableGpus[:i], avaliableGpus[i+1:]...)
				break
			}
		}
	}

	if len(newLeased) != len(oldLeased) {
		log.Warn("unable to match old GPUs to new GPUs, clearing all lxd GPUs")
		clearAllGpus()
		return
	}

	gpuState.statsManager.attachGpus <- newLeased

	gpuState.LeasedGpus = newLeased
	gpuState.AvaliableGpus = avaliableGpus
}
