package main

import (
	"gitlab.com/gartnera/golib/log"
	"github.com/coreos/go-systemd/activation"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func getUnixListener() net.Listener {
	listeners, err := activation.Listeners()
	if err != nil {
		panic(err)
	}
	if len(listeners) == 1 {
		return listeners[0]
	}

	sockPath := getSockPath()
	unixListener, err := net.Listen("unix", sockPath)
	if err != nil {
		panic(err)
	}
	os.Chmod(sockPath, 0666)
	return unixListener
}

func main() {
	m := InitStatsManager()
	manageGpus := true
	err := InitGpuManager(m)
	if err != nil {
		manageGpus = false
	}
	router := GetRouter(manageGpus)

	server := http.Server{
		Handler:   router,
		ConnState: pidMapper.ConnStateHandler,
	}
	unixListener := getUnixListener()
	go server.Serve(unixListener)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGTERM)

	// Block until a signal is received.
	s := <-c
	log.Warn("Got signal: ", s)

	m.shutdown <- true
	ShutdownGpuManager()
	server.Close()
	unixListener.Close()
}
