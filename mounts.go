package main

import (
	"fmt"
	"github.com/shirou/gopsutil/process"
	"io/ioutil"
	"os/exec"
	"strconv"
	"strings"
)

func stringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func maybeUmount(pid int32, rawMounts string, names []string) {
	mounts := strings.Split(rawMounts, "\n")
	for _, mount := range mounts {
		if len(mount) < 1 {
			continue
		}
		n := strings.Split(mount, " ")[1]
		if stringInSlice(n, names) {
			pidStr := strconv.Itoa(int(pid))
			
			// lxd
			exec.Command("nsenter", "-t", pidStr, "-m", "-p", "-U", "umount", "-l", n).Run()
			// docker
			exec.Command("nsenter", "-t", pidStr, "-m", "-p", "umount", "-fl", n).Run()
		}
	}
}

// sweepUmount is used to ensure no mounts remain after release (docker, nesting, etc)
func sweepUmount(names []string) {
	pids, _ := process.Pids()
	for _, pid := range pids {
		mountFile := fmt.Sprintf("/proc/%d/mounts", pid)
		res, err := ioutil.ReadFile(mountFile)
		if err != nil {
			return
		}
		maybeUmount(pid, string(res), names)
	}
}
