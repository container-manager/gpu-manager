package main

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/gartnera/golib/log"
)

func routeGet(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	countStr := r.URL.Query().Get("count")
	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	count64, err := strconv.ParseUint(countStr, 10, 32)
	if err != nil {
		msg := "count must be an integer"
		l.Info(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}
	count := uint(count64)

	lockStr := r.URL.Query().Get("lock")
	lock, err := strconv.ParseBool(lockStr)
	if err != nil {
		l.Info("invalid lock")
		http.Error(w, "lock must be a bool", http.StatusBadRequest)
		return
	}

	duration := getDefaultDuration()
	durationStr := r.URL.Query().Get("timeout")
	if durationStr != "" {
		d, err := time.ParseDuration(durationStr)
		if err != nil {
			l.Info("invalid timeout str")
			http.Error(w, "timeout not valid", http.StatusBadRequest)
		}
		duration = d
	}

	restartTimeoutStr := r.URL.Query().Get("restart-on-timeout")
	restartOnTimeout, err := strconv.ParseBool(restartTimeoutStr)
	if err != nil {
		l.Info("invalid restart-on-timeout")
		http.Error(w, "restart-on-timeout must be a bool", http.StatusBadRequest)
		return
	}

	err = AttachGpus(container, count, duration, restartOnTimeout, lock)
	if err != nil {
		errStr := err.Error()
		l.WithError(err).Info("failed to attach GPUs")
		http.Error(w, errStr, http.StatusConflict)
	} else {
		l.Info()
	}
}

func routeWait(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	countStr := r.URL.Query().Get("count")
	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	count64, err := strconv.ParseUint(countStr, 10, 32)
	if err != nil {
		msg := "count must be an integer"
		l.Info(msg)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}
	count := uint(count64)

	err = WaitGpus(container, count, w)
	if err != nil {
		l = l.WithError(err)
		http.Error(w, err.Error(), http.StatusConflict)
	}
	l.Info()
}

func routeRelease(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	err := ReleaseGpus(container)

	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})

	if err != nil {
		errStr := err.Error()
		l.WithError(err).Info()
		http.Error(w, errStr, http.StatusConflict)
	} else {
		l.Info()
	}
}

func routeLeases(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)

	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	l.Info()

	w.Header().Set("Content-Type", "application/json")
	w.Write(GetLeasesJSON())
}

func routeDone(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	err := SendDoneMessage(container)
	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	if err != nil {
		errStr := err.Error()
		l.WithError(err).Error()
		http.Error(w, errStr, http.StatusConflict)
	} else {
		l.Info()
	}
}

func routeSmi(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	output, err := GetHostSmiOutput()
	if err != nil {
		errStr := err.Error()
		l.WithError(err).Error()
		http.Error(w, errStr, http.StatusConflict)
	}
	l.Info()
	w.Write(output)
}

func routeSmiTopo(w http.ResponseWriter, r *http.Request) {
	container := GetContainerName(w)
	l := log.WithFields(log.Fields{
		"url":       r.URL,
		"container": container,
	})
	output, err := GetHostSmiTopoOutput()
	if err != nil {
		errStr := err.Error()
		l.WithError(err).Error()
		http.Error(w, errStr, http.StatusConflict)
	}
	l.Info()
	w.Write(output)
}

func GetRouter(manageGpus bool) http.Handler {
	router := mux.NewRouter()

	if manageGpus {
		router.HandleFunc("/get", routeGet)
		router.HandleFunc("/wait", routeWait)
		router.HandleFunc("/release", routeRelease)
		router.HandleFunc("/leases", routeLeases)
		router.HandleFunc("/smi", routeSmi)
		router.HandleFunc("/smi/topo", routeSmiTopo)
	}

	router.HandleFunc("/done", routeDone)

	recoveryOption := handlers.PrintRecoveryStack(true)
	handler := handlers.RecoveryHandler(recoveryOption)(router)

	return handler
}
