package main

import (
	"errors"
	"fmt"
	"github.com/lxc/lxd/client"
	"github.com/nlopes/slack"
	"os"
)

func SendDoneMessage(internalContainerName string) error {
	server, err := lxd.ConnectLXDUnix("", nil)
	if err != nil {
		return err
	}

	container, _, err := server.GetContainer(internalContainerName)
	if err != nil {
		return err
	}
	userName := container.Config["user.username"]
	containerName := container.Config["user.remotename"]
	msg := fmt.Sprintf("Your job running on *%s* is done", containerName)
	return sendUserPlainMessage(msg, userName)
}

func sendUserPlainMessage(message string, userName string) error {
	api, domain, err := getSlackApi()
	if err != nil {
		return err
	}

	email := fmt.Sprintf("%s@%s", userName, domain)
	user, err := api.GetUserByEmail(email)
	if err != nil {
		return err
	}

	api.PostMessage(user.ID, slack.MsgOptionText(message, false))

	return nil
}

func getSlackApi() (*slack.Client, string, error) {
	domain := os.Getenv("DOMAIN")
	if domain == "" {
		return nil, "", errors.New("DOMAIN not configured")
	}

	token := os.Getenv("SLACK_API_KEY")
	if token == "" {
		return nil, "", errors.New("SLACK_API_KEY not configured")
	}

	return slack.New(token), domain, nil
}
