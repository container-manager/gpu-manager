package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"sync"
	"unsafe"

	"golang.org/x/sys/unix"
)

func getSockPath() string {
	path := "/run/gpu-manager/socket"
	env := os.Getenv("GPUMANAGER_SOCK")
	if env != "" {
		path = env
	}
	return path
}

func GetContainerName(w http.ResponseWriter) string {
	conn := extractUnderlyingConn(w)
	cred, ok := pidMapper.m[conn]
	if !ok {
		panic("Cred lookup failed")
	}
	name := containerNameFromPid(cred.pid)
	if name == "" {
		panic("Couldn't lookup container")
	}
	return name
}

func containerNameFromPid(pid int32) string {
	file := fmt.Sprintf("/proc/%d/cgroup", pid)
	line := getLastLine(file)

	// regex for lxc ~3
	pat := regexp.MustCompile(`^0::/lxc(?:.payload)?/([a-zA-z0-9\-]+)`)
	matches := pat.FindStringSubmatch(line)
	if len(matches) == 2 {
		return matches[1]
	}

	// regex for lxc ~4
	pat = regexp.MustCompile(`^0::/lxc\.payload\.([^/]+)/`)
	matches = pat.FindStringSubmatch(line)
	if len(matches) == 2 {
		return matches[1]
	}

	return ""
}

func getLastLine(filename string) string {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	reader := bufio.NewReader(file)
	var lastLine string
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			return lastLine
		}
		lastLine = line
	}
}

/*
* Everything below here is the guts of the unix socket bits. Unfortunately,
* golang's API does not make this easy. What happens is:
*
* 1. We install a ConnState listener on the http.Server, which does the
*    initial unix socket credential exchange. When we get a connection started
*    event, we use SO_PEERCRED to extract the creds for the socket.
*
* 2. We store a map from the connection pointer to the pid for that
*    connection, so that once the HTTP negotiation occurrs and we get a
*    ResponseWriter, we know (because we negotiated on the first byte) which
*    pid the connection belogs to.
*
* 3. Regular HTTP negotiation and dispatch occurs via net/http.
*
* 4. When rendering the response via ResponseWriter, we match its underlying
*    connection against what we stored in step (2) to figure out which container
*    it came from.
 */

/*
* We keep this in a global so that we can reference it from the server and
* from our http handlers, since there appears to be no way to pass information
* around here.
 */
var pidMapper = ConnPidMapper{m: map[*net.UnixConn]*ucred{}}

type ucred struct {
	pid int32
	uid int64
	gid int64
}

type ConnPidMapper struct {
	m     map[*net.UnixConn]*ucred
	mLock sync.Mutex
}

func (m *ConnPidMapper) ConnStateHandler(conn net.Conn, state http.ConnState) {
	unixConn := conn.(*net.UnixConn)
	switch state {
	case http.StateNew:
		cred, err := getCred(unixConn)
		if err != nil {
			log.Printf("Error getting ucred for conn %s", err)
		} else {
			m.mLock.Lock()
			m.m[unixConn] = cred
			m.mLock.Unlock()
		}
	case http.StateActive:
		return
	case http.StateIdle:
		return
	case http.StateHijacked:
		/*
		* The "Hijacked" state indicates that the connection has been
		* taken over from net/http. This is useful for things like
		* developing websocket libraries, who want to upgrade the
		* connection to a websocket one, and not use net/http any
		* more. Whatever the case, we want to forget about it since we
		* won't see it either.
		 */
		m.mLock.Lock()
		delete(m.m, unixConn)
		m.mLock.Unlock()
	case http.StateClosed:
		m.mLock.Lock()
		delete(m.m, unixConn)
		m.mLock.Unlock()
	default:
		log.Printf("Unknown state for connection %s", state)
	}
}

/*
* I also don't see that golang exports an API to get at the underlying FD, but
* we need it to get at SO_PEERCRED, so let's grab it.
 */
func extractUnderlyingFd(unixConnPtr *net.UnixConn) (int, error) {
	conn := reflect.Indirect(reflect.ValueOf(unixConnPtr))

	netFdPtr := conn.FieldByName("fd")
	if !netFdPtr.IsValid() {
		return -1, fmt.Errorf("Unable to extract fd from net.UnixConn")
	}
	netFd := reflect.Indirect(netFdPtr)

	fd := netFd.FieldByName("sysfd")
	if !fd.IsValid() {
		// Try under the new name
		pfdPtr := netFd.FieldByName("pfd")
		if !pfdPtr.IsValid() {
			return -1, fmt.Errorf("Unable to extract pfd from netFD")
		}
		pfd := reflect.Indirect(pfdPtr)

		fd = pfd.FieldByName("Sysfd")
		if !fd.IsValid() {
			return -1, fmt.Errorf("Unable to extract Sysfd from poll.FD")
		}
	}

	return int(fd.Int()), nil
}

func getUcred(fd int) (uint32, uint32, int32, error) {
	cred, err := unix.GetsockoptUcred(fd, unix.SOL_SOCKET, unix.SO_PEERCRED)
	if err != nil {
		return 0, 0, -1, err
	}

	return cred.Uid, cred.Gid, cred.Pid, nil
}

func getCred(conn *net.UnixConn) (*ucred, error) {
	fd, err := extractUnderlyingFd(conn)
	if err != nil {
		return nil, err
	}

	uid, gid, pid, err := getUcred(fd)
	if err != nil {
		return nil, err
	}

	return &ucred{pid, int64(uid), int64(gid)}, nil
}

/*
* As near as I can tell, there is no nice way of extracting an underlying
* net.Conn (or in our case, net.UnixConn) from an http.Request or
* ResponseWriter without hijacking it [1]. Since we want to send and receive
* unix creds to figure out which container this request came from, we need to
* do this.
*
* [1]: https://groups.google.com/forum/#!topic/golang-nuts/_FWdFXJa6QA
 */
func extractUnderlyingConn(w http.ResponseWriter) *net.UnixConn {
	v := reflect.Indirect(reflect.ValueOf(w))
	connPtr := v.FieldByName("conn")
	conn := reflect.Indirect(connPtr)
	rwc := conn.FieldByName("rwc")

	netConnPtr := (*net.Conn)(unsafe.Pointer(rwc.UnsafeAddr()))
	unixConnPtr := (*netConnPtr).(*net.UnixConn)

	return unixConnPtr
}
