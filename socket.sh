#!/bin/bash

set -x

chmod 777 socket
lxc config device remove $1 socket
lxc config device remove $1 cli
lxc config device add $1 socket disk source=$(pwd)/socket path=/run/gpu-manager/socket

lxc config device add $1 cli disk source=$(pwd)/cli/cli path=/usr/bin/gpu
