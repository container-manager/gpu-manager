package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"time"
)

type StatsGpu struct {
	Username  string `json:"username,omitempty"`
	Container string `json:"container,omitempty"`
	Lock      bool   `json:"lock,omitempty"`
}

type GpuInfoMsg struct {
	GPU map[uint]string `json:"gpu"`
}

type GpuStatsMsg struct {
	GPU map[uint]StatsGpu `json:"gpu"`
}

type HostStatsMsg struct {
	CPU    uint `json:"cpu"`
	Memory uint `json:"memory"`
	Disk   uint `json:"disk"`
}

type InfoMsg struct {
	Hostname string      `json:"hostname"`
	Info     interface{} `json:"info"`
}

type ShutdownMsg struct {
	Hostname string `json:"hostname"`
}

type StatsMsg struct {
	Hostname string      `json:"hostname"`
	Stats    interface{} `json:"stats"`
}

type StatsManager struct {
	initGpu     chan []string
	attachGpus  chan []*Gpu
	releaseGpus chan []*Gpu
	shutdown    chan bool
	hostname    string
	server      string
	httpClient  *http.Client
}

func (m *StatsManager) start() {
	for {
		otherStatsInterval := time.After(20 * time.Second)
		select {
		case names := <-m.initGpu:
			gpuMap := make(map[uint]string)
			gpuInfo := GpuInfoMsg{
				GPU: gpuMap,
			}

			for i, name := range names {
				i := uint(i)
				gpuMap[i] = name
			}

			msg := InfoMsg{
				Hostname: m.hostname,
				Info:     gpuInfo,
			}
			m.post("/init", msg)
		case leases := <-m.attachGpus:
			gpuMap := make(map[uint]StatsGpu)
			gpuStats := GpuStatsMsg{
				GPU: gpuMap,
			}

			for _, lease := range leases {
				stat := StatsGpu{
					Username:  lease.UserName,
					Container: lease.ContainerName,
					Lock:      lease.Lock,
				}
				gpuMap[lease.Idx] = stat
			}

			msg := StatsMsg{
				Hostname: m.hostname,
				Stats:    gpuStats,
			}
			m.post("/stat", msg)
		case leases := <-m.releaseGpus:
			gpuMap := make(map[uint]StatsGpu)
			gpuStats := GpuStatsMsg{
				GPU: gpuMap,
			}

			for _, lease := range leases {
				stat := StatsGpu{}
				gpuMap[lease.Idx] = stat
			}

			msg := StatsMsg{
				Hostname: m.hostname,
				Stats:    gpuStats,
			}
			m.post("/stat", msg)
		case <-otherStatsInterval:
			msg := StatsMsg{
				Hostname: m.hostname,
				Stats:    buildHostStatsMsg(),
			}
			m.post("/stat", msg)
		case <-m.shutdown:
			msg := ShutdownMsg{
				Hostname: m.hostname,
			}
			m.post("/shutdown", msg)
			return
		}
	}
}

func (m *StatsManager) discardStats() {
	for {
		select {
		case <-m.initGpu:
		case <-m.attachGpus:
		case <-m.releaseGpus:
		case <-m.shutdown:
			return
		}
	}

}

func (m *StatsManager) post(path string, data interface{}) {
	url := m.server + path
	jsonBytes, _ := json.Marshal(data)
	resp, err := m.httpClient.Post(url, "application/json", bytes.NewBuffer(jsonBytes))
	if err == nil {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}
}

func InitStatsManager() *StatsManager {
	statsServer := os.Getenv("STATS_SERVER")
	hostname, _ := os.Hostname()
	m := &StatsManager{
		initGpu:     make(chan []string, 10),
		attachGpus:  make(chan []*Gpu, 10),
		releaseGpus: make(chan []*Gpu, 10),
		shutdown:    make(chan bool),
		hostname:    hostname,
		server:      statsServer,
	}
	if statsServer != "" {
		m.httpClient = &http.Client{
			Timeout: time.Second * 5,
		}
		go m.start()
	} else {
		fmt.Fprintln(os.Stderr, "Warning: no stats server configured")
		go m.discardStats()
	}
	return m
}

type LVMReport struct {
	Report []struct {
		Lv []struct {
			LvName          string  `json:"lv_name"`
			VgName          string  `json:"vg_name"`
			LvAttr          string  `json:"lv_attr"`
			LvSize          string  `json:"lv_size"`
			PoolLv          string  `json:"pool_lv"`
			Origin          string  `json:"origin"`
			DataPercent     float64 `json:"data_percent,string"`
			MetadataPercent float64 `json:"metadata_percent,string"`
			MovePv          string  `json:"move_pv"`
			MirrorLog       string  `json:"mirror_log"`
			CopyPercent     string  `json:"copy_percent"`
			ConvertLv       string  `json:"convert_lv"`
		} `json:"lv"`
	} `json:"report"`
}

func buildHostStatsMsg() HostStatsMsg {
	cores := float64(runtime.NumCPU())
	loadAvg, _ := load.Avg()
	load1 := float64(loadAvg.Load1)

	usage := uint(0)

	if load1 > cores {
		usage = 100
	} else {
		usage = uint(load1 / cores * 100)
	}

	mem, _ := mem.VirtualMemory()
	memUsage := uint(mem.UsedPercent)

	output, err := exec.Command("lvs", "-q", "--reportformat=json", "vg/pool").Output()
	if err != nil {
		fmt.Println(string(output))
		panic(err)
	}

	report := LVMReport{}
	json.Unmarshal(output, &report)

	poolReport := report.Report[0].Lv[0]
	diskUsage := poolReport.DataPercent
	if poolReport.MetadataPercent > diskUsage {
		diskUsage = poolReport.MetadataPercent
	}

	msg := HostStatsMsg{
		CPU:    usage,
		Memory: memUsage,
		Disk:   uint(diskUsage),
	}

	return msg
}
